//
//  CommonVC.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import UIKit
import SCLAlertView
class CommonVC: UIViewController {
    
    
    var heightKeyBoard: CGFloat = 0 
    var failed: Failure!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.failed = { () -> Void in
            self.showServerError()
        }
    
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NOTIFICATION_CENTER.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name:  NSNotification.Name.UIKeyboardWillShow, object: nil)
        NOTIFICATION_CENTER.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        NOTIFICATION_CENTER.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NOTIFICATION_CENTER.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func showServerError() {
        MBProgressHUD.hideAllHUDs(for: self.view, animated:  true)
        
        let appearance = SCLAlertView.SCLAppearance (    
            kTitleFont: UIFont(name: "HelveticaNeue", size: 20)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false
        )
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") { 
            print("ABC")
        }
        alertView.showWarning("No Connection", subTitle: "Please Check Connection")
    }
    
    // là func dùng chung cho việc hiện userMessage trong các trường hợp status của API trả về < 0
    func showUserMessgeOfDataResult(data1: AnyObject?) {
        var message =  "Error! Please try again"
        
        if let data = data1 as? [String: AnyObject] {
            if let dataResult = data["data"] as? [String: AnyObject] {
                if let m = dataResult["userMessage"] as? String {
                    message = m
                }
            }
            if let m = data["data"] as? String {
                message = m
            }
        }
        
        let appearance = SCLAlertView.SCLAppearance (    
            showCloseButton: false
        )
        
        let alertView = SCLAlertView(appearance: appearance)
        
        alertView.addButton("OK") { 
            print("ABC")
        }
        alertView.showWarning("Error Warning", subTitle: message)
        
    }
    
    //MARK: NotificationCenter methods: cần override + super để nhận biết khi nào showKeyBoard
    func keyboardWillShow(notification: NSNotification) {
        let userInfo: [AnyHashable : Any] = notification.userInfo as [AnyHashable : Any]!
        let keyBoardFrame: NSValue = userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
        let keyBoardCGRect = keyBoardFrame.cgRectValue
        
        self.heightKeyBoard = keyBoardCGRect.height
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let userInfo: [AnyHashable : Any] = notification.userInfo as [AnyHashable : Any]!
        let keyBoardFrame: NSValue = userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue
        let keyBoardCGRect = keyBoardFrame.cgRectValue
        self.heightKeyBoard = keyBoardCGRect.height
    }
}
