//
//  Functions.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import Foundation


func convertToNSData(data: AnyObject)-> NSData {
    return NSKeyedArchiver.archivedData(withRootObject: data) as NSData
}

func convertToAnyObject(data: NSData) -> AnyObject {
    return NSKeyedUnarchiver.unarchiveObject(with: data as Data)! as AnyObject
}


func getIntValue(_ object: AnyObject?, errorValue: Int = INT_WRONG, didError: (()-> Void)? = nil) -> Int {
    if object == nil || object is NSNull {
        return errorValue
    }
    
    if object is String {
        if let int = Int(object as! String) {
            return int
        }
    }
    
    if object is NSNumber {
        return object as! Int
    }
    
    if object is Int {
        return object as! Int
    }
    
    didError?()
    return errorValue
}


func getStringValue(object: AnyObject?, errorValue: String = STRING_WRONG, didError: (()-> Void)? = nil) -> String {
    if object == nil || object is NSNull {
        return errorValue
    }
    
    if object is String {
        return object as! String
    }
    
    if object is NSNumber {
        return (object as! NSNumber).stringValue
    }
    
    if object is Int {
        return "\(object!)"
    }
    
    print("DID GET STRING ERROR")
    didError?()
    return errorValue
}
