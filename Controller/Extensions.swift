//
//  Extensions.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView {
    func setImageWithURL(str: String, imagePlaceholder: UIImage) {
        let url: URL = URL(string: str)!        
        self.kf.setImage(with: url,
                         placeholder: imagePlaceholder,
                         options: [.transition(.fade(1))],
                         progressBlock: nil,
                         completionHandler: nil)
    }
    
    func setImageWithURL1(str: String) {
        let url: URL = URL(string: BASE_URL_IMAGE + str)!
        self.kf.setImage(with: url,
                         placeholder: IMAGE_DEFAULT,
                         options: [.transition(.fade(1))],
                         progressBlock: nil,
                         completionHandler: nil)
    }
    
    func setImageWithURL2(str: String, imagePlaceholder: UIImage?) {
        let url: URL = URL(string: BASE_URL_IMAGE + str)!
        self.kf.setImage(with: url,
                         placeholder: imagePlaceholder,
                         options: [.transition(.fade(1))],
                         progressBlock: nil,
                         completionHandler: nil)
        
    }
    
}
