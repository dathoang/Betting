//
//  Constant.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import Foundation
import UIKit

let STRING_WRONG: String = "ERROR STRING"
let INT_WRONG: Int = 10000000
let BASE_URL_IMAGE: String = ""
let IMAGE_DEFAULT: UIImage = UIImage(named: "")!



//Keywords
let SHARE_APPLICATION: UIApplication = UIApplication.shared
let NOTIFICATION_CENTER: NotificationCenter = NotificationCenter.default
let USER_DEFAULTS: UserDefaults = UserDefaults.standard


//Constants
let HEIGHT_SCREEN: CGFloat = UIScreen.main.bounds.height
let WIDTH_SCREEN: CGFloat = UIScreen.main.bounds.width
var IS_IPHONE = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
