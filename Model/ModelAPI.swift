//
//  ModelAPI.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

typealias Success = (_ result: AnyObject) -> ()
typealias Failure = () -> ()

class ModelAPI: NSObject {
    class func handleResponse(response: DataResponse<Any>, success: Success, failure: Failure, parameters: [String : AnyObject]) {
        if response.result.isSuccess {
            if let result = response.result.value as? [String : AnyObject]{
                if getIntValue(result["status"]) > 0 {
                    success(response.result.value! as AnyObject)
                    print((response.request?.url)! as URL)
                    print(parameters)
                } else {
                    print("status: ", getStringValue(object: result["status"]))
                    print("\n ERROR status < 0")
                    print(result)
                    print((response.request?.url)! as URL)
                    success(response.result.value! as AnyObject)
                }
            } else {
                failure()
                print("ERROR result ERROR")
                print(response.result.value as AnyObject)
            }
        } else {
            print("ERROR \(response.result.error?.localizedDescription)")
            print("ERROR URL \(response.response?.url)")
            print("ERROR param \(parameters)")
            
            failure()
        }
    }
}


extension ModelAPI {
    static func request(method: HTTPMethod, URLString: URLConvertible, parameter: [String: AnyObject]?, encoding: ParameterEncoding, headers: [String: String]? ) -> DataRequest{
        
        return Alamofire.request(URLString, method: method, parameters: parameter, encoding: encoding, headers: nil)
    }
}


extension ModelAPI {
    class func API_Post(param: [String: AnyObject], success: @escaping Success, failure: @escaping Failure) {
        let url = ""
        ModelAPI.request(method: .post, URLString: url, parameter: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            handleResponse(response: response, success: success, failure: failure, parameters: param)
        } 
    }
    
    class func API_Get(param: [String: AnyObject], success: @escaping Success, failure: @escaping Failure) {
        let url = ""
        ModelAPI.request(method: .get, URLString: url, parameter: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            handleResponse(response: response, success: success, failure: failure, parameters: param)
        }
    }
}
