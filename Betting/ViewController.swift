//
//  ViewController.swift
//  Betting
//
//  Created by Đạt Hoàng Văn on 2/21/17.
//  Copyright © 2017 Đạt Hoàng Văn. All rights reserved.
//

import UIKit

class ViewController: CommonVC {

    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func keyboardWillHide(notification: NSNotification) {
        print("hide")
    }
    override func keyboardWillShow(notification: NSNotification) {
        print("show")
    }
}

